import * as React from 'react';
import {useContext} from 'react';
import {SafeAreaView, ScrollView, StyleSheet, View} from 'react-native';
import Header from "../components/header";
import BottomTabs from "../components/bottom-tabs";
import {Book, Props} from "../types";
import {observer} from "mobx-react-lite";
import BooksStore from "../observables/book-store";
import BookInMyLibrary from "../components/book-in-my-library";

const style = StyleSheet.create({
  container: {flex: 1, backgroundColor: "#FFFFFF"},
  content: {flexGrow: 1, paddingHorizontal: 16}
});

function MyLibraryScreen({navigation}: Props) {
  const store = useContext(BooksStore);
  const {booksInMyLibrary} = store;

  return (
    <SafeAreaView style={style.container}>
      <Header title="My Library"/>
      <ScrollView contentContainerStyle={style.content}>
        {
          booksInMyLibrary.map((book: Book) => <BookInMyLibrary key={"book-"+Math.random()} book={book} navigation={navigation}/>)
        }
      </ScrollView>
      <BottomTabs current="MyLibrary" navigation={navigation}/>
    </SafeAreaView>
  );
}

export default observer(MyLibraryScreen)
