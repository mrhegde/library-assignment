import * as React from 'react';
import {useEffect} from 'react';
import {Image, SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {Props} from "../types";
import {useContext} from "react";
import BooksStore from "../observables/book-store";

const style = StyleSheet.create({
  container: {flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: "#FFFFFF"},
  image: {width: "75%",},
  text24: {fontSize: 24},
  welcomeCaption: {},
});


export default function SplashScreen({navigation}: Props) {
  const store = useContext(BooksStore);
  useEffect(() => {
    store.init()
    const timer = setTimeout(() => navigation.replace('Home'), 3000);
    return () => clearTimeout(timer);
  }, []);

  return (
    <SafeAreaView style={style.container}>
      <Image
        style={style.image}
        source={require("../../assets/images/books-stack.png")}
      />
      <Text>
        <Text>Welcome to </Text>
        <Text style={style.text24}>MyLib. </Text>
        <Text>Your online library</Text>
      </Text>

    </SafeAreaView>
  );


}
