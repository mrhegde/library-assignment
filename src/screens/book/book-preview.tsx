import React, {useContext, useEffect, useState} from 'react';
import {Alert, Image, Platform, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import BooksStore from "../../observables/book-store";
import {Book, Props} from "../../types";
import {observer} from "mobx-react";
import {Icon} from "react-native-elements";

const style = StyleSheet.create({
  container: {flex: 1, backgroundColor: "#ffffff"},
  header: {
    height: 60,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 16,
    width: "100%",
    alignItems: "center",
    backgroundColor: "#FFFFFF"
  },
  backArrow: {
    fontSize: 32,
  },
  shareIcon: {
    fontSize: 32
  },
  loading: {
    width: "100%"
  },
  thumbnail: {
    width: 180,
    height: 250,
    alignItems: "center",
    justifyContent: "flex-end"
  },
  ratingsContainer: {
    // color:"#FF865D"
    flexDirection: "row"
  },
  ratingText: {
    color: "#109EFF"
  },
  bookBrief: {
    paddingHorizontal: 32,
    alignItems: "center",
    paddingBottom: 80
  },
  title: {
    paddingVertical: 16,
    fontSize: 24,
    fontWeight: "bold",
    textAlign: "center"
  },
  author: {
    paddingBottom: 16,
    fontSize: 16,
    fontWeight: "bold"
  },
  brief: {
    color: "#707070",
    textAlign: "center"
  },
  readNowButton: {
    height: 60,
    padding: 12,
    backgroundColor: "#FF4A06",
    width: 150,
    alignSelf: "center",
    borderRadius: 30,
    alignItems: "center",
    justifyContent: "center",
    bottom: 16,
    position: 'absolute',
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  readNowText: {
    color: "#FFFFFF",
    fontSize: 16,
    fontWeight: "bold"
  },
});

function BookPreviewScreen({route, navigation}: Props) {
  const store = useContext(BooksStore);
  const {find, isInMyLibrary, addBookToMyLibrary, removeBookFromMyLibrary, setBookViewing} = store;
  const [book, setBook] = useState(null as Book | null);
  const onBookError = () => navigation.pop();
  useEffect(() => {
    let book = find(route.params.bookId);
    if (book == undefined) {
      Alert.alert("Error", "The book you are trying open could not be found",
        [{text: "Back to home", onPress: onBookError, style: 'cancel'}]);
    } else {
      setBookViewing(book.id);
      setBook(book);
    }
  });
  const saveIconColor = isInMyLibrary ? "#FF865D" : "#000000";
  if (book == null) {
    return (<SafeAreaView style={{flex: 1, alignItems: "center", justifyContent: "center"}}>
      <Text>LOADING BOOK...</Text>
    </SafeAreaView>);
  }

  return (<SafeAreaView style={style.container}>
    <View style={style.header}>
      <Icon type="material" name="chevron-left" color="#FF865D" iconStyle={style.backArrow}
            onPress={() => navigation.pop()}/>
      <Icon type="material" name="share" color="#FF865D" iconStyle={style.shareIcon}
            onPress={() => {
            }}/>
    </View>
    <ScrollView contentContainerStyle={{flexGrow: 1, alignItems: "center"}}>
      <Image source={{uri: book.thumbnail}} style={style.thumbnail}/>
      <Icon name='bookmark' iconStyle={{fontSize: 48}} color={saveIconColor}
            onPress={() => isInMyLibrary ? removeBookFromMyLibrary(book.id) : addBookToMyLibrary(book.id)}/>
      <View style={style.ratingsContainer}>
        <Text style={style.ratingText}>Book Rating: </Text>
        <Text style={style.ratingText}>{book.ratings.toString()}</Text>
        <Icon name="star" color="#109EFF" style={{marginTop: 2}} iconStyle={{fontSize: 14}}/>
      </View>
      <View style={style.bookBrief}>
        <Text style={style.title}>{book.title}</Text>
        <Text style={style.author}>By: {book.author.name}</Text>
        <Text style={style.brief}>{book.contents[0].substr(0, 200)}...</Text>
      </View>
    </ScrollView>

    <TouchableOpacity
      style={style.readNowButton}
      onPress={() => navigation.navigate("ReadBook", {bookId: book.id})}>
      <Text style={style.readNowText}>Read Now</Text>
    </TouchableOpacity>
  </SafeAreaView>);
}

export default observer(BookPreviewScreen)
