import React, {useContext, useEffect, useState} from 'react';
import {Alert, SafeAreaView, ScrollView, StyleSheet, Text, View} from "react-native";
import BooksStore from "../../observables/book-store";
import {Book, Props} from "../../types";
import {observer} from "mobx-react-lite";
import {Icon} from "react-native-elements";
import BottomSheet from 'react-native-bottomsheet';

const lightTheme = StyleSheet.create({
  bg: {backgroundColor: "#FFFFFF"},
  text: {color: "#242424"}
});
const darkTheme = StyleSheet.create({
  bg: {backgroundColor: "#242424"},
  text: {color: "#FFFFFF"}
});

const style = StyleSheet.create({
  container: {flex: 1},
  content: {flexGrow: 1, padding: 16},
  header: {
    height: 60,
    flexDirection: "row",
    justifyContent: "flex-end",
    paddingHorizontal: 16,
    width: "100%",
    alignItems: "center",
  },
  backArrow: {
    fontSize: 32,
  },
  shareIcon: {
    fontSize: 32
  },
  bottomNav: {
    height: 40,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 16,
    alignItems: "center"
  },
  pageContent: {
    flexWrap: "wrap",
    textAlign: "left",
    fontWeight: "600",
    lineHeight: 22
  },

});

function ReadBookScreen({route, navigation}: Props) {
  const store = useContext(BooksStore);
  const {find, setOpenBook, isInMyLibrary, addBookToMyLibrary, removeBookFromMyLibrary, saveCurrentPageOfBook} = store;
  const [book, setBook] = useState(undefined as Book | undefined);
  const [darkMode, setDarkMode] = useState(false);
  const onBookError = () => navigation.pop();

  useEffect(() => {
    setOpenBook(route.params.bookId);
    let book = find(route.params.bookId);
    if (book == undefined) {
      Alert.alert("Error", "The book you are trying open could not be found",
        [{text: "Back to home", onPress: onBookError, style: 'cancel'}]);
    } else {
      setBook(book);
    }
  });


  const pageNext = async () => {
    await saveCurrentPageOfBook(book!.id, book!.currentPage + 1);
  };
  const pagePrev = async () => {
    await saveCurrentPageOfBook(book!.id, book!.currentPage - 1);
  };
  const openMenu = () => {
    BottomSheet.showBottomSheetWithOptions({
      options: [isInMyLibrary ? 'Remove from my library' : 'Add to my library', 'Save and Close Book', darkMode ? 'Switch to Light Mode' : 'Switch to Dark Mode', 'Cancel'],
      title: 'Reading Menu',
      dark: darkMode,
      cancelButtonIndex: 3,
    }, async (value) => {
      switch (value) {
        case 0:
          isInMyLibrary ? removeBookFromMyLibrary(book!.id) : addBookToMyLibrary(book!.id);
          break;
        case 1:

          navigation.pop();
          break;
        case 2:
          setDarkMode(!darkMode);
          break;
      }
    });
  };

  if (book == undefined) {
    return (<SafeAreaView style={{flex: 1, alignItems: "center", justifyContent: "center"}}>
      <Text>LOADING BOOK...</Text>
    </SafeAreaView>)
  }
  return (<SafeAreaView style={[style.container, darkMode ? darkTheme.bg : lightTheme.bg]}>
    <ScrollView contentContainerStyle={[style.content, darkMode ? darkTheme.bg : lightTheme.bg]}>
      <Text
        style={[style.pageContent, darkMode ? darkTheme.text : lightTheme.text, {paddingVertical: 16}]}>Chapter: {book.currentPage + 1}</Text>
      <Text
        style={[style.pageContent, darkMode ? darkTheme.text : lightTheme.text]}>{book.contents[book.currentPage]}</Text>
    </ScrollView>
    <View style={[style.bottomNav, darkMode ? darkTheme.bg : lightTheme.bg]}>
      <Icon type="material" name="chevron-left"
            disabledStyle={[darkMode ? darkTheme.bg : lightTheme.bg]}
            disabled={book.currentPage == 0}
            color={book.currentPage == 0 ? "#707070" : "#FF865D"}
            onPress={pagePrev}/>
      <Icon type="material" name="menu"
            color="#FF865D"
            iconStyle={style.shareIcon}
            onPress={openMenu}/>
      <Icon type="material" name="chevron-right"
            color={book.currentPage == (book.contents.length - 1) ? "#707070" : "#FF865D"}
            disabled={book.currentPage == book.contents.length - 1}
            disabledStyle={[darkMode ? darkTheme.bg : lightTheme.bg]}
            onPress={pageNext}/>
    </View>
  </SafeAreaView>)
}

export default observer(ReadBookScreen)
