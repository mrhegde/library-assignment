import * as React from 'react';
import {useContext, useEffect, useState} from 'react';
import {FlatList, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Header from "../components/header";
import BottomTabs from "../components/bottom-tabs";
import {Book, Props} from "../types";
import SearchBarComponent from "../components/search";
import BooksStore from "../observables/book-store";
import BookInShelf from "../components/book-in-shelf";
import {observer} from 'mobx-react-lite';
import RecentlyOpened from '../components/recently-opened'

const style = StyleSheet.create({
  container: {flex: 1, alignItems: 'flex-start', justifyContent: 'flex-start', backgroundColor: "#FFFFFF"},
  content: {alignItems: 'flex-start', justifyContent: 'flex-start', flexGrow: 1},
  tabScrollContainer: {
    paddingVertical: 8,
    marginLeft: 16,
    height: 60,
    width: "100%",
  },
  tabHeader: {
    marginRight: 32,
    fontSize: 18,
    fontWeight: "bold",
    color: "#000000"
  },
  tabHeaderActive: {
    color: "#FF865D"
  },
  noRecentBook: {height: 150, width: "100%", alignItems: "center", justifyContent: "center"},
  startReading: {fontSize: 16, fontWeight: "bold", color: "#707070"},
  caption: {fontSize: 12, color: "#707070"}
});

function HomeScreen({navigation}: Props) {
  const store = useContext(BooksStore);
  const [recentlyOpened, setRecentlyOpened] = useState(null as Book | null)
  const {books, tags, tag, setTag, openBook} = store;

  const changeTab = (tag: string) => {
    setTag(tag);
  };
  useEffect(() => {
    if (openBook != undefined)
      setRecentlyOpened(openBook)
  })

  const renderTabHeader = ({item}: any) => (
    <TouchableOpacity onPress={() => changeTab(item)} key={item}>
      <Text style={[style.tabHeader, tag == item ? style.tabHeaderActive : {}]}>{item}</Text>
    </TouchableOpacity>);

  const renderBookPreview = ({item}: any) => {
    return (<BookInShelf navigation={navigation} book={item}/>)
  };

  return (
    <SafeAreaView style={{flex:1, backgroundColor:"#ffffff"}}>
    <View style={style.container}>
      <Header title="Explore"/>
      <SearchBarComponent/>
      <ScrollView contentContainerStyle={style.content}>
        <View style={{height: 60}}>
          <FlatList
            style={{paddingStart: 16}}
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            scrollEnabled={true}
            data={tags}
            renderItem={renderTabHeader}
            keyExtractor={item => item}
          />
        </View>
        <FlatList
          contentContainerStyle={{height: 210,}}
          style={{paddingStart: 16}}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          scrollEnabled={true}
          data={books}
          renderItem={renderBookPreview}
          keyExtractor={(item: Book) => item.id.toString()}
        />
        {
          recentlyOpened != null ? <RecentlyOpened book={recentlyOpened!} navigation={navigation}/> : (
            <View style={style.noRecentBook}>
              <Text style={style.startReading}>Start Reading!</Text>
              <Text style={style.caption}>Your recent book will appear here</Text>
            </View>)
        }

      </ScrollView>
      <BottomTabs current="Home" navigation={navigation}/>
    </View>
    </SafeAreaView>
  );
}

export default observer(HomeScreen);
