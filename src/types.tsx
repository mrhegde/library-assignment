import {StackNavigationProp} from "@react-navigation/stack/lib/typescript/src/types";
import {RouteProp} from '@react-navigation/native';

type RootStackParamList = { Home: undefined, MyLibrary: undefined, Profile: undefined, BookPreview: { bookId: number }, ReadBook: { bookId: number }, };
type BookPreviewParamsList = { book: { bookId: number }; }
type MainNavigationProp = StackNavigationProp<RootStackParamList>
export type Props = { navigation: MainNavigationProp, route: RouteProp<BookPreviewParamsList, 'book'> };
export type HeaderOptions = { title: string }
export type BottomTabOptions = { current: string, navigation: MainNavigationProp }
type Author = { name: string, bio: string, picture: string }
export type Book = { id: number, author: Author, "country": string, "thumbnail": string, "language": string, "link": string, "pages": number, "title": string, "year": number, ratings: number, currentPage: number, tag: string, contents: string[] }
export type BookInShelfOptions = { book: Book, navigation: MainNavigationProp }
