import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from "./screens/home";
import SplashScreen from "./screens/splash";
import MyLibraryScreen from "./screens/my-library";
import BookPreviewScreen from "./screens/book/book-preview";
import ReadBookScreen from "./screens/book/read";

const RootStack = createStackNavigator();
const App = () => {
  return (<NavigationContainer>{
    <RootStack.Navigator headerMode="none" initialRouteName="splash">
      <RootStack.Screen name="splash" component={SplashScreen}/>
      <RootStack.Screen name="Home" component={HomeScreen} options={{animationEnabled: false,}}/>
      <RootStack.Screen name="MyLibrary" component={MyLibraryScreen} options={{animationEnabled: false,}}/>
      <RootStack.Screen name="BookPreview" component={BookPreviewScreen}/>
      <RootStack.Screen name="ReadBook" component={ReadBookScreen}/>
    </RootStack.Navigator>
  }</NavigationContainer>);
};
export default App;
