import React from 'react'
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {BookInShelfOptions} from "../types";

const style = StyleSheet.create({
  container: {
    width: "100%",
    height: 150,
    backgroundColor: "#FFFFFF",
    marginRight: 16,
    shadowColor: "#000000",
    alignItems: "flex-start",
    flexDirection: "row",
    marginVertical: 8
  },
  thumbnail: {
    width: 120,
    height: 150,
    borderRadius: 8,
    backgroundColor: "#BFBFBF"
  },
  textContainer: {
    paddingHorizontal: 16,
    flexWrap: "wrap",
    flex: 1
  },
  title: {
    flexWrap: "wrap",
    color: "#000000",
    width: "100%",
    textAlign: "left",
    fontSize: 16,
    fontWeight: "bold",
    marginVertical: 8,
  },
  author: {
    color: "#707070",
    textAlign: "left",
    width: "100%",
    fontWeight: "bold"
  }

});

export default function BookInMyLibrary({book, navigation}: BookInShelfOptions) {

  return (
    <TouchableOpacity onPress={() => navigation.navigate("BookPreview", {bookId: book.id})}>
      <View style={style.container}>
        <Image source={{uri: book.thumbnail}} style={style.thumbnail}/>
        <View style={style.textContainer}>
          <Text style={style.title}>Book: {book.title}</Text>
          <Text style={style.author}>By: {book.author.name}</Text>
          <Text style={style.author}>Year: {book.year}</Text>
          <Text style={style.author}>Reading
            Completion: {Math.round((book.currentPage / (book.contents.length - 1)) * 100).toString()}%</Text>
        </View>
      </View>
    </TouchableOpacity>

  )
}
