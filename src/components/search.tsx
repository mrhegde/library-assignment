import React, {useContext} from 'react'
import {StyleSheet, TextInput, View} from "react-native";
import {Icon} from "react-native-elements";
import BooksStore from "../observables/book-store";
import {observer} from "mobx-react-lite";

const style = StyleSheet.create({
  container: {
    width: "100%",
    padding: 24,
  },
  inputView: {
    backgroundColor: "#EEEEEE",
    borderRadius: 32,
    paddingHorizontal: 16,
    height: 50,
    flexDirection: "row",
    alignItems: "center"
  },
  input: {
    flex: 1,
  }
});
 function SearchBarComponent() {
  const {setSearchText} = useContext(BooksStore);
  return (
    <View style={style.container}>
      <View style={style.inputView}>
        <Icon name="search" color="#A2A2A2"/>
        <TextInput style={style.input} onChangeText={text => setSearchText(text.toString())}
                   placeholder="Search for book name or author."/>
      </View>
    </View>
  )
}
export default observer(SearchBarComponent);
