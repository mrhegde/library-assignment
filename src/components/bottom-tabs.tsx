import React from 'react';
import {Platform, StyleSheet, TouchableOpacity, View} from 'react-native'
import {BottomTabOptions} from "../types";
import {Icon} from "react-native-elements";

const style = StyleSheet.create({
  shadowIos:{},
  shadowAndroid:{
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: -16,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.00,
    elevation: 20,
  },
  container: {
    height: 60,
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: 'space-between',
    backgroundColor: "#FFFFFF",
    paddingHorizontal: 32,
  },

});
const iconColors = {
  active: "#FF865D",
  inActive: "#000000",
};

export default function BottomTabs({current, navigation}: BottomTabOptions) {
  return (
    <View style={style.container}>
      <TouchableOpacity style={{paddingHorizontal:16}} onPress={() => current == "Home" ? null : navigation.navigate('Home')}>
        <Icon name="explore" color={current == "Home" ? iconColors.active : iconColors.inActive}/>
      </TouchableOpacity>
      <TouchableOpacity  style={{paddingHorizontal:16}} onPress={() => current == "MyLibrary" ? null : navigation.navigate('MyLibrary')}>
        <Icon name="bookmark" color={current == "MyLibrary" ? iconColors.active : iconColors.inActive}/>
      </TouchableOpacity>
    </View>
  )
}
