import React from 'react';
import {StyleSheet, Text, View} from 'react-native'
import {HeaderOptions} from "../types";

const style = StyleSheet.create({
  container: {
    height: 60,
    width: "100%",
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: 'space-between',
    backgroundColor: "#FFFFFF",
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    letterSpacing: 4,
    color: "#FF865D"
  }
});

export default function Header({title}: HeaderOptions) {
  return (
    <View style={style.container}>
      <Text style={style.title}>{title}</Text>
    </View>
  );
}
