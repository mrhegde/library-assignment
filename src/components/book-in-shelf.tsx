import React from 'react'
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {BookInShelfOptions} from "../types";

const style = StyleSheet.create({
  container: {
    width: 150,
    height: 200,
    backgroundColor: "#FFFFFF",
    marginRight: 16,
    shadowColor: "#000000",
    alignItems: "flex-start",
  },
  thumbnail: {
    width: 120,
    height: 150,
    borderRadius: 8,
    backgroundColor: "#BFBFBF"
  },
  title: {
    color: "#000000",
    width: "100%",
    textAlign: "left",
    fontSize: 14,
    fontWeight: "bold",
    marginVertical: 8
  },
  author: {
    color: "#707070",
    textAlign: "left",
    width: "100%",
    fontSize:11,
    fontWeight: "bold"
  }
});

export default function BookInShelf({book, navigation}: BookInShelfOptions) {

  return (
    <TouchableOpacity onPress={() => navigation.navigate("BookPreview", {bookId: book.id})}>
      <View style={style.container}>
        <Image source={{uri: book.thumbnail}} style={style.thumbnail}/>
        <Text style={style.title}>{book.title.substr(0,20)}</Text>
        <Text style={style.author}>{book.author.name}</Text>
      </View>
    </TouchableOpacity>

  )
}
