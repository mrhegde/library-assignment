import React from 'react'
import {BookInShelfOptions} from "../types";
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {observer} from 'mobx-react-lite'

const style = StyleSheet.create({
  container: {
    height: 150,
    width: "100%",
    alignItems: "center",
    justifyContent: "flex-start",
    paddingBottom: 16
  },
  content: {
    width: "80%",
    height: "100%",
    borderRadius: 8,
    backgroundColor: "#FFFFFF",
    justifyContent: "flex-start",
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  bookDetailsContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    padding: 8,
  },
  title: {
    flexWrap: "wrap",
    color: "#000000",
    textAlign: "left",
    fontSize: 16,
    fontWeight: "bold",
    marginVertical: 8,
  },
  thumbnail: {
    height: 80,
    width: 60,
    borderRadius: 4
  }
});

function RecentlyOpened({navigation, book}: BookInShelfOptions) {

  return (
    <TouchableOpacity style={style.container} onPress={() => navigation.navigate("BookPreview", {bookId: book.id})}>
      <View style={style.content}>
        <Text style={{padding: 8, color: "#808080"}}>Recently Opened</Text>
        <View style={style.bookDetailsContainer}>
          <Image source={{uri: book.thumbnail}} style={style.thumbnail}/>
          <View style={{paddingHorizontal: 16}}>
            <Text style={style.title}>{book.title}</Text>
            <Text>Completion: {Math.round((book.currentPage / (book.contents.length - 1)) * 100).toString()}%</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>)
}

export default observer(RecentlyOpened)
