import {action, computed, observable} from 'mobx'
import {Book} from "../types";
import {createContext} from "react";
import AsyncStorage from "@react-native-community/async-storage";

const booksData = require('../../assets/books.json');

class BooksStore {
  @observable _myLibrary: number[] = [];
  @observable _searchText: string = '';
  @observable _openBook?: number;
  @observable _bookViewing: number = 0;
  @observable _tag: string = "";
  tags = ["Best Sellers", "New Arrivals", "Coming Soon"];

  @observable _books: Book[] = [];

  @computed get books(): Book[] {
    if (0 < this._searchText.length) {
      return this._books.filter(book => {
        return book.tag == this._tag && (book.title.toLocaleLowerCase().includes(this._searchText.toLocaleLowerCase()) || book.author.name.toLocaleLowerCase().includes(this._searchText.toLocaleLowerCase()))
      });
    }
    return this._books.filter(book => book.tag == this._tag);
  }

  @computed get tag(): string {
    return this._tag;
  }

  @computed get booksInMyLibrary(): Book[] {
    return this._books.filter((book: Book) => this._myLibrary.includes(book.id))
  }

  @computed get isInMyLibrary(): boolean {
    return -1 < this._myLibrary.indexOf(this._bookViewing)
  };

  @computed get openBook() {
    if (this._openBook)
      return this._books.find((book: Book) => book.id == this._openBook)
  }

  @action setOpenBook = async (id: number): Promise<void> => {
    this._openBook = id;
    await AsyncStorage.setItem("openBook", id.toString());
  };
  @action setBookViewing = async (id: number): Promise<void> => {
    this._bookViewing = id;
  };

  @action find = (id: number): Book | undefined => this._books.find((book: Book) => book.id == id);

  @action init = async (): Promise<void> => {
    try {

      let json = booksData.map((book: any) => {
        return {...book, author: {...book.author}}
      });

      this._books = json.map((j: any): Book => j);
      this._tag = this.tags[0];
      let myLib = await AsyncStorage.getItem("myLib");
      let openBook = await AsyncStorage.getItem("openBook");
      this._openBook = openBook != null ? parseInt(openBook) : 0;
      if (myLib != null) {
        let myLibJson = JSON.parse(myLib);
        this._myLibrary = myLibJson.map((j: any): number => j);
      }
    } catch (error) {
      console.log(error);
    }
  };

  @action addBookToMyLibrary = async (bookId: number): Promise<void> => {
    let _myLibrary = this._myLibrary;
    _myLibrary.push(bookId);
    try {
      await AsyncStorage.setItem("myLib", JSON.stringify(this._myLibrary));
      this._myLibrary = _myLibrary;
    } catch (error) {
      console.log(error)
    }
  };
  @action removeBookFromMyLibrary = async (bookId: number): Promise<void> => {
    this._myLibrary = this._myLibrary.filter((id: number) => id !== bookId);
    try {
      await AsyncStorage.setItem("myLib", JSON.stringify(this._myLibrary));
    } catch (error) {
      console.log(error)
    }
  };
  @action saveCurrentPageOfBook = async (bookId: number, page: number): Promise<void> => {
    this._books = this.books.map((book: Book) => {
      if (book.id === bookId)
        book.currentPage = page;
      return book;
    });
    try {
      await AsyncStorage.setItem(
        'books',
        JSON.stringify(this._books)
      );
    } catch (e) {
      console.log(e);
    }
  };

  @action setSearchText = (text: string): void => {
    this._searchText = text;
  };
  @action setTag = (tag: string): void => {
    this._tag = tag;
  };

}

export default createContext(new BooksStore())
